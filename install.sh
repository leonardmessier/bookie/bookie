#!/bin/bash

declare -ir ERROR_PARSE_OPTS=1
declare -ir ERROR_NOT_ROOT=2
declare -ir ERROR_MISSING_DEP=3

if ! OPTS=$(getopt -o vhns: --long archive-url:,archive-top-folder:,ignore-dependencies, -n 'parse-options' -- "$@"); then
    echo -e "Failed parsing options." >&2
    exit $ERROR_PARSE_OPTS
fi

eval set -- "$OPTS"

IGNORE_DEPENDENCIES=false
ARCHIVE_URL="https://gitlab.com/leonardmessier/bookie/bookie/-/archive/master/bookie-master.tar.gz"
ARCHIVE_TOP_FOLDER="bookie-master"

while true; do
    case "$1" in
        --ignore-dependencies ) IGNORE_DEPENDENCIES=true; shift; shift ;;
        --archive-url ) ARCHIVE_URL=$2; shift; shift ;;
        --archive-top-folder ) ARCHIVE_TOP_FOLDER=$2; shift; shift ;;
        * ) break;;
    esac
done

_is_root() {
    ID="$(id -u)"
    if [ "$ID" == 0 ]; then
        return 0
    else
        return 1
    fi
}

_is_installed() {
    local DEP=$1
    if command -v "$DEP" > /dev/null 2>&1; then
      return 0
    else
      return 1
    fi
}

configure() {
    local DEPS=("jq" "git")

    if _is_root; then
        echo "This should not be run as root"
        return $ERROR_NOT_ROOT
    fi

    if [ "$IGNORE_DEPENDENCIES" == true ]; then
        return 0
    fi

    for DEP in "${DEPS[@]}"
    do
        if ! _is_installed "$DEP"; then
            echo "$DEP is not installed. Exiting"
            return $ERROR_MISSING_DEP
        fi
    done
}

download() {
    local FILE_PATH=$2
    local ARCHIVE_URL=$1
    curl --silent -o "$FILE_PATH" "$ARCHIVE_URL"
}

extract() {
    ARCHIVE_PATH=$1
    DESTINATION=$2

    tar xzf "$ARCHIVE_PATH" -C "$DESTINATION"
}

install_binary() {
    SOURCE=$1

    if [ -d /opt/bookie ]; then
        sudo rm -r /opt/bookie
    fi

    sudo cp -r "$SOURCE" /opt/bookie
    sudo chmod +x /opt/bookie/bin/bookie
    sudo ln -f -s /opt/bookie/bin/bookie /usr/local/bin/bookie
}

configure
CONFIGURED=$?
if [ "$CONFIGURED" -ne 0 ]; then
    exit $CONFIGURED
fi

download "$ARCHIVE_URL" /tmp/bookie-master.tar.gz
extract /tmp/bookie-master.tar.gz /tmp
install_binary "/tmp/$ARCHIVE_TOP_FOLDER"

