# Bookie

Everything bookmarks

## Install

Run the following running install script

```sh
$ /bin/bash -c "$(curl -fsSL https://gitlab.com/CaptainQuirk/bookie/-/raw/master/install.sh)"
```

## Initialize

**You have to initialize a git repository to hold the buku database**

```sh
$ bookie init \
    --user "John Doe" \
    --email "john@doe.com" \
    --origin {origin}
    --path {path}
```

Where `{origin}` is the clone url of the git repo you want to use to store the
bookmark database and `{path} the path on your machine you want to use as
local repository.
