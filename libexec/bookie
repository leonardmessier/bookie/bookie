#!/usr/bin/env bash
set -e

resolve_link() {
  $(type -p greadlink readlink | head -1) "$1"
}

abs_dirname() {
  local path="$1"
  local cwd

  cwd="$(pwd)"
  while [ -n "$path" ]; do
    cd "${path%/*}"
    local name="${path##*/}"
    path="$(resolve_link "$name" || true)"
  done

  pwd
  cd "$cwd"
}

libexec_path="$(abs_dirname "$0")"
_BOOKIE_ROOT="$(abs_dirname "$libexec_path")"
export _BOOKIE_ROOT
export PATH="${libexec_path}:$PATH"

command="$1"
case "$command" in
"" | "-h" | "--help" )
  exec bookie-help
  ;;
* )
  command_path="$(command -v "bookie-$command" || true)"
  if [ ! -x "$command_path" ]; then
    echo "bookie: no such command \`$command'" >&2
    exit 1
  fi

  shift
  exec "$command_path" "$@"
  ;;
esac
