if [[ ! -o interactive ]]; then
    return
fi

compctl -K _bookie bookie

_bookie() {
  local word words completions
  read -cA words
  word="${words[2]}"

  if [ "${#words}" -eq 2 ]; then
    completions="$(bookie commands)"
  else
    completions="$(bookie completions "${word}")"
  fi

  reply=("${(ps:\n:)completions}")
}
