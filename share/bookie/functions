#!/bin/bash

_export_buku_db() {
    buku --print --json > "$HOME/.config/shef/configuration/buku/buku.json"
}

_read_cached_bookmarks_count() {
    cat "$HOME/.config/shef/configuration/buku/.buku-count"
}

_read_current_bookmarks_count() {
    jq '. | length' "$HOME/.config/shef/configuration/buku/buku.json"
}

_write_cached_bookmarks_count() {
    local COUNT
    COUNT="$(jq '. | length' "$HOME/.config/shef/configuration/buku/buku.json")"

    echo "$COUNT" > "$HOME/.config/shef/configuration/buku/.buku-count"
}

_get_modified_or_deleted_entry() {
    git diff -U0 | grep '^[+-]' | grep -Ev '^(--- a/|\+\+\+ b/)' | sed 's/^.//'
}

_something_change() {
    cd "$HOME/.config/shef/configuration/buku/" || exit 99
    if git diff-index --quiet HEAD --; then
        echo nothing changed
        cd - || exit 99
        return 1
    else
        git status
        echo something changed
        cd - || exit 99
        return 0
    fi
}

_what_changed() {
    CURRENT_BOOKMARKS_COUNT="$(_read_current_bookmarks_count)"
    CACHED_BOOKMARKS_COUNT="$(_read_cached_bookmarks_count)"

    if [ "$CURRENT_BOOKMARKS_COUNT" -gt "$CACHED_BOOKMARKS_COUNT" ]; then
        echo 'added'
    elif [ "$CURRENT_BOOKMARKS_COUNT" -lt "$CACHED_BOOKMARKS_COUNT" ]; then
        echo 'deleted'
    else
        echo 'modified'
    fi
}

_get_message() {
    WHAT_CHANGED="$(_what_changed)"

    case $WHAT_CHANGED in
        added)
            TITLE=$(jq '.[-1].title' "$HOME/.config/shef/configuration/buku/buku.json")
            MESSAGE="Adding $TITLE bookmark"
            ;;
        deleted)
            ENTRY="$(_get_modified_or_deleted_entry)"
            TITLE="$(echo "$ENTRY" | jq '.title')"
            MESSAGE="Removing $TITLE bookmark"
            ;;
        modified)
            ENTRY="$(_get_modified_or_deleted_entry)"
            TITLE="$(echo "$ENTRY" | jq '.title')"
            MESSAGE="Changing $TITLE bookmark"
            ;;
    esac

    echo "$MESSAGE"
}

_commit_and_push_change() {
    MESSAGE="$(_get_message)"

    if ! _something_change; then
        return
    fi

    cd "$HOME/.config/shef/configuration/buku/" || exit 99
    git add buku.json
    git commit -m "$MESSAGE"

    if git push origin master; then
        notify-send "$MESSAGE. Git remote up to date."
    fi
    cd - || exit 99
}
